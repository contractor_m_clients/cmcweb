﻿using Cmc.Infrastructure.Data.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace Cmc.Web.Register
{
    public static class InMemoryConfig
    {
        public static void Configurate(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IUserRepository, InMemoryUserRepository>();
        }
    }
}
