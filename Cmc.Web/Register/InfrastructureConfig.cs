﻿using Cmc.Core.Interfaces;
using Cmc.Infrastructure.Data;
using Cmc.Infrastructure.Data.Connection;
using Cmc.Infrastructure.Data.Connection.Providers;
using Microsoft.Extensions.DependencyInjection;

namespace Cmc.Web.Register
{
    public static class InfrastructureConfig
    {
        public static void Configurate(IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IConnectionStringProvider, PostgreSQLConnectionProvider>();
            serviceCollection.AddTransient<IUnitOfWorkFactory, BaseUnitOfWorkFactory>();
            serviceCollection.AddTransient<IUnitOfWork, BaseUnitOfWork>();
        }
    }
}
