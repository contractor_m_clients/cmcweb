﻿using Cmc.Interactors;
using Cmc.Interactors.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Cmc.Web.Register
{
    public static class InteractorsConfig
    {
        public static void Configurate(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IUserInteractor, UserInteractor>();
        }
    }
}
