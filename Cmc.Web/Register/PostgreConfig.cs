﻿using Cmc.Infrastructure.Data.Repositories;
using Cmc.Infrastructure.Data.Repositories.Postgre;
using Microsoft.Extensions.DependencyInjection;

namespace Cmc.Web.Register
{
    public static class PostgreConfig
    {
        public static void Configurate(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IUserRepository, PostgreUserRepository>();
        }
    }
}
