﻿using System.ComponentModel.DataAnnotations;

namespace Cmc.Web.Models
{
    // Note: doesn't expose events or behavior
    public class UserDto
    {
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsDone { get; private set; }
    }
}
