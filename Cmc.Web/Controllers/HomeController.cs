﻿using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Cmc.Web.Base;
using Cmc.Interactors.Interfaces;
using Cmc.Interactors;

namespace Cmc.Web.Controllers
{
    public partial class HomeController : BaseController
    {
        private readonly IUserInteractor _userInteractor;

        public HomeController(IMapper mapper, IUserInteractor userInteractor) : base(mapper)
        {
            _userInteractor = userInteractor;
        }

        public IActionResult Index()
        {
            var userRequest = new AddUserRequest() { };
            var response = _userInteractor.Add(new AddUserRequest());
            return View(response);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
