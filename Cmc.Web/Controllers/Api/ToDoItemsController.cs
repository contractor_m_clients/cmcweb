﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Cmc.Core.Interfaces;
using Cmc.Web.Models;
using Cmc.Web.Filters;
using Cmc.Entities.DBEntities;

namespace Cmc.Web.Controllers.Api
{
    [Route("api/[controller]")]
    [ValidateModel]
    public partial class ToDoController : Controller
    {
        private readonly IWriteRepository<User> _todoRepository;

        public ToDoController(IWriteRepository<User> todoRepository)
        {
            _todoRepository = todoRepository;
        }

        // GET: api/ToDoItems
        [HttpGet]
        public IActionResult List()
        {
            //var items = _todoRepository.All
            //                .Select(item => UserDto.FromToDoItem(item));
            //return Ok(items);
            return Ok();
        }

        // GET: api/ToDoItems
        [HttpGet("{id:int}")]
        public IActionResult GetById(int id)
        {
            //var item = UserDto.FromToDoItem(_todoRepository.GetById(id));
            //return Ok(item);
            return Ok();
        }

        // POST: api/ToDoItems
        public async Task<IActionResult> Post([FromBody] UserDto item)
        {
            //var todoItem = new User()
            //{
            //    Title = item.Title,
            //    Description = item.Description
            //};
            //_todoRepository.Add(todoItem);
            //return Ok(UserDto.FromToDoItem(todoItem));
            return Ok();
        }
    }
}
