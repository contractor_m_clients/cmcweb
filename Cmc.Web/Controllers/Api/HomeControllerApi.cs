﻿using AutoMapper;
using Cmc.Web.Base;

namespace Cmc.Web.Controllers.Api
{
    public partial class HomeController : BaseController
    {
        public HomeController(IMapper mapper) : base(mapper) { }
    }
}
