﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Cmc.Core.Interfaces;
using Cmc.Entities.DBEntities;

namespace Cmc.Web.Controllers
{
    public class ToDoController : Controller
    {
        private readonly IWriteRepository<User> _todoRepository;

        public ToDoController(IWriteRepository<User> todoRepository)
        {
            _todoRepository = todoRepository;
        }

        public IActionResult Index()
        {
            return View(_todoRepository.All.ToList());
        }

        public IActionResult Populate()
        {
            int recordsAdded = PopulateDatabase();
            return Ok(recordsAdded);
        }

        public int PopulateDatabase()
        {
            if (_todoRepository.All.Count() > 0) return 0;
            _todoRepository.Add(new User()
            {
                FirstName = "Get Sample Working",
                LastName = "Try to get the sample to build."
            });
            _todoRepository.Add(new User()
            {
                FirstName = "Review Solution",
                LastName = "Review the different projects in the solution and how they relate to one another."
            });
            _todoRepository.Add(new User()
            {
                FirstName = "Run and Review Tests",
                LastName = "Make sure all the tests run and review what they are doing."
            });
            return 3;
        }
    }
}
