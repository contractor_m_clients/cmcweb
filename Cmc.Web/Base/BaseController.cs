﻿using Microsoft.AspNetCore.Mvc;
using AutoMapper;

namespace Cmc.Web.Base
{
    public class BaseController : Controller
    {
        private readonly IMapper _mapper;

        IMapper Mapper { get { return _mapper; } }

        public BaseController(IMapper mapper) : base()
        {
            _mapper = mapper;
        }
    }
}
