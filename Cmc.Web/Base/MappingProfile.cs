﻿using AutoMapper;
using Cmc.Entities.DBEntities;
using Cmc.Web.Models;

namespace Cmc.Web.Base
{
    public class MappingProfile : Profile
    {
        public MappingProfile() : base()
        {
            CreateMap<User, UserDto>();
            CreateMap<UserDto, User>();
        }
    }
}
