﻿using System.ComponentModel.DataAnnotations;
using Cmc.Entities.Base;

namespace Cmc.Entities.DBEntities
{
    public class GroupUser : BaseEntity
    {
        [Key] 
        public int GroupId { get; set; }
        [Key]
        public string UserId { get; set; }

        public virtual Group Group { get; set; }
        public virtual User User  { get; set; }
    }
}
