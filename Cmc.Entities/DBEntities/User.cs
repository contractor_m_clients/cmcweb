﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using Cmc.Entities.Base;
using Cmc.Core.Interfaces;

namespace Cmc.Entities.DBEntities
{
    public class User : IdentityUser
    {
        public List<BaseDomainEvent> Events = new List<BaseDomainEvent>();
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CoverImg { get; set; }
        public bool Executor { get; set; }

        public virtual ICollection<Project> Projects { get; set; }
        public virtual ICollection<Bet> Bets { get; set; }
        public virtual ICollection<GroupUser> GroupUsers { get; set; }
        public virtual ICollection<Message> Messages { get; set; }
        public virtual ICollection<PrivateMessage> PrivateMessages { get; set; }

        public bool IsDone { get; private set; } = false;

        public void MarkComplete()
        {
            IsDone = true;            
        }
    }
}