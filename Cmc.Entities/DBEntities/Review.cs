﻿using Cmc.Entities.Base;
using System.Collections.Generic;

namespace Cmc.Entities.DBEntities
{
    public class Review : BaseIntKeyEntity
    {
        public int ProjectId { get; set; }
        public string UserId { get; set; }
        public string ReviewText { get; set; }

        public virtual Project Project { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<ReviewGrade> ReviewGrades { get; set; }
    }
}
