﻿using Cmc.Entities.Base;
using System.Collections.Generic;

namespace Cmc.Entities.DBEntities
{
    public class Country : BaseIntKeyEntity
        {
            public string Name { get; set; }
            public virtual ICollection<Region> Regions { get; set; }
        }
}
