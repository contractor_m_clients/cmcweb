﻿using Cmc.Entities.Base;
using System.Collections.Generic;

namespace Cmc.Entities.DBEntities
{
    public class Category : BaseIntKeyEntity
    {
        public string Name { get; set; }
        public virtual ICollection<Project> Project { get; set; }
        }
}
