﻿using System.Collections.Generic;
using Cmc.Entities.Base;

namespace Cmc.Entities.DBEntities
{
    public class Region : BaseIntKeyEntity
    {
        public int CountryId { get; set; }
        public string Name { get; set; }
        public virtual Country Country { get; set; }
        public virtual ICollection<Project> Projects { get; set; }
    }
}
