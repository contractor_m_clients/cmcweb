﻿using Cmc.Entities.Base;

namespace Cmc.Entities.DBEntities
{
    public class PortfolioPhoto : BaseIntKeyEntity
    {
 
        public int PortfolioId { get; set; }
        public string Photo { get; set; }
        public string Cover { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Header { get; set; }
        public string Alt { get; set; }
        public virtual Portfolio Portfolio { get; set; }
    }
}
