﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Cmc.Entities.Base;

namespace Cmc.Entities.DBEntities
{
    public class Bet : BaseTrackingEntity
    {
     
        public string UserId { get; set; }
        public int ProjectId { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }

        [Column(TypeName = "Money")]
        public decimal Cost { get; set; }
        public int Duration { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Win { get; set; }

        public virtual User User { get; set; }
        public virtual Project Project { get; set; }
    }
}
