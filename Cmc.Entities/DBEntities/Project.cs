﻿using System.Collections.Generic;
using Cmc.Entities.Base;

namespace Cmc.Entities.DBEntities
{
    public enum State
    {
        Published = 0,
        Rate = 10,
        Approved = 20,
        Reserved = 30,
        Implemented = 40,
        Finalized = 50,
        Rewiew = 60,
        Closed = 70
    }

    public class Project : BaseTrackingEntity
    {
        public string UserId { get; set; }
        public int RegionId { get; set; }
        public int CategoryId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int EstimatePeriod { get; set; }
        public string City { get; set; }
        public string Location { get; set; }
        public string ContactEmail { get; set; }
        public string ContactPhone { get; set; }
        public State State { get; set; }

        public virtual Region Region { get; set; }
        public virtual User User { get; set; }
        public virtual Category Category { get; set; }
        public virtual ICollection<PrivateProjectMessage> PrivateProjectMessages { get; set; }
    }
}
