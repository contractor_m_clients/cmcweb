﻿using System.ComponentModel.DataAnnotations;
using Cmc.Entities.Base;

namespace Cmc.Entities.DBEntities
{
    public class ReviewGrade : BaseEntity
    {
        [Key]
        public int ReviewId { get; set; }
        [Key]
        public int GradeId { get; set; }
        public int Value { get; set; }

        public virtual Review Review { get; set; }
        public virtual Grade Grade { get; set; }
    }
}
