﻿using System.ComponentModel.DataAnnotations;
using Cmc.Entities.Base;

namespace Cmc.Entities.DBEntities
{
    public class PrivateProjectMessage : BaseEntity
    {
        [Key]
        public int MessageId { get; set; }
        [Key]
        public int ProjectId { get; set; }
        public virtual Message Message { get; set; }
        public virtual Project Project { get; set; }
    }
}
