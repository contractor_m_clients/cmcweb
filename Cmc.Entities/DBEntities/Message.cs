﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Cmc.Entities.Base;

namespace Cmc.Entities.DBEntities
{
    public enum MessageType
    {
        Message = 100, Event = 200, Notification = 300
    }

    public class Message : BaseIntKeyEntity
    {
        public string UserId { get; set; }
        public int ParentId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public MessageType MessageType { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime PostDate { get; set; }

        public virtual User User { get; set; }
        public virtual ICollection<PrivateMessage> PrivateMessage { get; set; }
        public virtual ICollection<PrivateProjectMessage> PrivateProjectMessage { get; set; }

    }
}
