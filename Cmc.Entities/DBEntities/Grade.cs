﻿using Cmc.Entities.Base;

namespace Cmc.Entities.DBEntities
{
    public class Grade : BaseIntKeyEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int MaxValue { get; set; }
    }
}
