﻿using System.ComponentModel.DataAnnotations;
using Cmc.Entities.Base;

namespace Cmc.Entities.DBEntities
{
    public class PrivateMessage : BaseEntity
    {
        [Key]
        public int MessageId { get; set; }
        [Key]
        public string UserId { get; set; }

        public virtual Message Message { get; set; }
        public virtual User User { get; set; }
    }
}
