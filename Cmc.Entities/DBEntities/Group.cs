﻿using Cmc.Entities.Base;

namespace Cmc.Entities.DBEntities
{
    public class Group : BaseIntKeyEntity
    {
        public string Name { get; set; }
    }
}
