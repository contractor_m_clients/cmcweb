﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Cmc.Entities.Base;

namespace Cmc.Entities.DBEntities
{
    public class Portfolio : BaseIntKeyEntity
    {
        public string UserId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime ReleaseDate { get; set; }
        public string Place { get; set; }

        public virtual User User { get; set; }
        public virtual ICollection<PortfolioPhoto> PortfolioPhotos { get; set; }
    }
}
