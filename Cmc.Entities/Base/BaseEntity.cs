﻿using Cmc.Core.Interfaces;
using System.Collections.Generic;

namespace Cmc.Entities.Base
{
    // This can be modified to BaseEntity<TId> to support multiple key types (e.g. Guid)
    public abstract class BaseEntity
    {
        public List<BaseDomainEvent> Events = new List<BaseDomainEvent>();
    }
}