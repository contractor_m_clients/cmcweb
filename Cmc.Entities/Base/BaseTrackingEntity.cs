﻿using System;

namespace Cmc.Entities.Base
{
    public abstract class BaseTrackingEntity : BaseIntKeyEntity
    {
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public bool Readed { get; set; }
    }
}
