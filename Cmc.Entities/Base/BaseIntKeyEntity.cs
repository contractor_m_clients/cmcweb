﻿using System.Collections.Generic;

namespace Cmc.Entities.Base
{
    public abstract class BaseIntKeyEntity : BaseEntity
    {
        public int Id { get; set; }
        
    }
}
