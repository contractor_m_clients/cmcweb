echo off
dotnet test Tests\Cmc.Test.Core\Cmc.Test.Core.csproj
dotnet test Tests\Cmc.Test.Services\Cmc.Test.Services.csproj
dotnet test Tests\Cmc.Test.Infrastructure\Cmc.Test.Infrastructure.csproj
dotnet test Tests\Cmc.Test.Web\Cmc.Test.Web.csproj
if not "%1"=="continue" pause