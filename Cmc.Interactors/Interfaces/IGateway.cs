﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cmc.Interactors.Interfaces
{
    public interface IGateway<TId, TEntity>
    {
        TEntity Get(TId id);
        TEntity Save(TEntity entity);        
    }
}
