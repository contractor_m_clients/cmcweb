﻿using Cmc.Core.Interfaces;

namespace Cmc.Interactors.Interfaces
{
    public interface IUserInteractor
    {
        IResponse<AddUserResponse> Add(AddUserRequest request);
        IResponse<UpdateUserResponse> Update(UpdateUserRequest request);
        IResponse<DeleteUserResponse> Delete(DeleteUserRequest request);
    }
}
