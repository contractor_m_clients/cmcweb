﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Cmc.Core.Interfaces;
using Cmc.Interactors.Interfaces;
using Cmc.Infrastructure.Data.DTO;

namespace Cmc.Interactors.Interactors.Message
{
    public interface IMessageInteractor
    {
        IQueryable Query { get; set; }

       // IQueryable Find(int id);

        void Add(MessageDTO requestObj, IPresenter<MessageDTO> responseObj);

        void Update(MessageDTO requestObj, IPresenter<MessageDTO> responseObj);

        void Delete(int requestObj, IPresenter<bool> responseObj);


    }
}
