﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Cmc.Core.Interfaces;
using Cmc.Interactors.Interfaces;
using Cmc.Infrastructure.Data.DTO;
using Cmc.Interactors.Interactors.Message;

namespace Cmc.Interactors.Interactors
{
    public class MessageInteractor : IMessageInteractor

    {
        public IQueryable Query { get; set; }
        

        public void Add(MessageDTO requestObj, IPresenter<MessageDTO> presenter)
        {
            presenter.View(new Response<MessageDTO> { Result = true, StatusCode = 200, Data = requestObj });
        }

        public void Update(MessageDTO requestObj, IPresenter<MessageDTO> presenter)
        {
            presenter.View(new Response<MessageDTO> { Result = true, StatusCode = 200, Data = requestObj });
        }

        public void Delete(int requestObj, IPresenter<bool> presenter)
        {
            presenter.View(new Response<bool> { Result = true, StatusCode = 200, Data = true });
        }


    }
}
