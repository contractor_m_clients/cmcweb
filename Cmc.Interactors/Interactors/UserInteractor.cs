﻿using Cmc.Interactors.Interfaces;
using Cmc.Core.Interfaces;
using Cmc.Infrastructure.Data.Repositories;
using Cmc.Entities.DBEntities;

namespace Cmc.Interactors
{
    public class UserInteractor : IUserInteractor
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public UserInteractor(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public IResponse<AddUserResponse> Add(AddUserRequest request)
        {
            using (var unitOfWork = _unitOfWorkFactory.CreateUnitOfWork())
            {
                var repo = unitOfWork.GetNewRepo<IUserRepository>();
                repo.Add(new User());
                return new Response<AddUserResponse>();
            }            
        }

        public IResponse<UpdateUserResponse> Update(UpdateUserRequest request)
        {
            return new Response<UpdateUserResponse>();
        }

        public IResponse<DeleteUserResponse> Delete(DeleteUserRequest request)
        {
            return new Response<DeleteUserResponse>();
        }
    }
}
