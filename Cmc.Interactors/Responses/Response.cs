﻿using System;
using System.Collections.Generic;
using System.Text;
using Cmc.Core.Interfaces;

namespace Cmc.Interactors
{
    public class Response<T> : IResponse<T>
    {
        public bool Result { get; set; }
        public int StatusCode { get; set; }
        public string Message { get; set; }
        public T Data { get; set; }
    }
}
