﻿using System;
using Cmc.Core.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Cmc.Infrastructure.Data
{
    public class BaseUnitOfWork : IUnitOfWork, IDisposable
    {
        private readonly AppDbContext _dbContext;
        private readonly IServiceProvider _provider;

        public BaseUnitOfWork(AppDbContext dbContext, IServiceProvider provider)
        {
            _dbContext = dbContext;
            _provider = provider;
        }

        public R GetNewRepo<R>()
        {
            return _provider.GetService<R>();
        }


        public void SaveChanges()
        {
            if (_dbContext.ChangeTracker.HasChanges())
            {
                using (var transaction = _dbContext.Database.BeginTransaction())
                {
                    try
                    {
                        _dbContext.SaveChanges();
                        transaction.Commit();
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                }                
            }
        }

        public void DiscartChanges()
        {
            if (_dbContext.ChangeTracker.HasChanges())
            {
                foreach(var entry in _dbContext.ChangeTracker.Entries())
                {
                    entry.Reload();
                }
            }
        }

        public void Dispose()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }
    }
}
