﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Cmc.Core.Interfaces;
using Cmc.Infrastructure.Data.Connection;
using Cmc.Entities.DBEntities;
using Cmc.Entities.Base;

namespace Cmc.Infrastructure.Data
{
    public class AppDbContext : IdentityDbContext<User>
    {
        private readonly IDomainEventDispatcher _dispatcher;
        private readonly IConnectionStringProvider _connectionStringProvider;
        
        public AppDbContext(DbContextOptions<AppDbContext> options, 
            IDomainEventDispatcher dispatcher, 
            IConnectionStringProvider connectionStringProvider)
            : base(options)
        {
            _dispatcher = dispatcher;
            _connectionStringProvider = connectionStringProvider;
        }

        public DbSet<Project> Projects { get; set; }
        public DbSet<Region> Regions { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<PrivateMessage> PrivateMessages { get; set; }
        public DbSet<PrivateProjectMessage> PrivateProjectMessages { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<GroupUser> GroupUsers { get; set; }
        public DbSet<Portfolio> Portfolios { get; set; }
        public DbSet<PortfolioPhoto> PortfolioPhotos { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<ReviewGrade> ReviewGrades { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<GroupUser>()
                .HasKey(c => new { c.GroupId, c.UserId });
            builder.Entity<PrivateMessage>()
                .HasKey(c => new { c.MessageId, c.UserId });
            builder.Entity<PrivateProjectMessage>()
                .HasKey(c => new { c.MessageId, c.ProjectId });
            builder.Entity<ReviewGrade>()
                .HasKey(c => new { c.ReviewId, c.GradeId });
        }

        public override int SaveChanges()
        {
            int result = base.SaveChanges();

            // dispatch events only if save was successful

            var entitiesWithEvents = ChangeTracker.Entries<BaseEntity>()
                .Select(e => e.Entity)
                .Where(e => e.Events.Any())
                .ToArray();

            foreach (var entity in entitiesWithEvents)
            {
                var events = entity.Events.ToArray();
                entity.Events.Clear();
                foreach (var domainEvent in events)
                {
                    _dispatcher.Dispatch(domainEvent);
                }
            }

            return result;
        }
    }
}