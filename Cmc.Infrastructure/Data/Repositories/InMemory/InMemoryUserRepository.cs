﻿using Cmc.Entities;
using Cmc.Entities.DBEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cmc.Infrastructure.Data.Repositories
{
    public class InMemoryUserRepository : WriteRepository<User>, IUserRepository
    {
        public InMemoryUserRepository(AppDbContext dbContext) : base(dbContext)
        {
            //TODO: Setup test data
        }
    }
}
