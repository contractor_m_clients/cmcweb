﻿using Cmc.Core.Interfaces;
using Cmc.Entities.DBEntities;

namespace Cmc.Infrastructure.Data.Repositories
{
    public interface IUserRepository: IWriteRepository<User>
    {
    }
}
