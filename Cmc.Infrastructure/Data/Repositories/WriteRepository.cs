﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using Cmc.Core.Interfaces;

namespace Cmc.Infrastructure.Data.Repositories
{
    public class WriteRepository<T> : ReadRepository<T>, IWriteRepository<T> where T : class
    {
        public WriteRepository(AppDbContext dbContext) : base(dbContext) { }

        public override IQueryable<T> All => Set().AsQueryable<T>();
        
        public T Add(T entity)
        {
            Set().Add(entity);
            
            return entity;
        }

        public void Delete(T entity)
        {
            Set().Remove(entity);            
        }

        public void Update(T entity)
        {
            Context.Entry(entity).State = EntityState.Modified;
        }
    }
}
