﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using Cmc.Core.Interfaces;

namespace Cmc.Infrastructure.Data.Repositories
{
    public class ReadRepository<T> : IReadRepository<T> where T : class
    {
        private readonly AppDbContext _dbContext;

        public ReadRepository(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public AppDbContext Context { get { return _dbContext; } }

        public virtual IQueryable<T> All => Set().AsNoTracking<T>();

        public T GetById(int id)
        {
            return Set().Find(id);
        }

        protected DbSet<T> Set()
        {
            return _dbContext.Set<T>();
        }
    }
}
