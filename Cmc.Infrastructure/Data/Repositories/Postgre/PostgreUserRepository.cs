﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cmc.Core.Interfaces;
using Cmc.Entities.DBEntities;

namespace Cmc.Infrastructure.Data.Repositories.Postgre
{
    public class PostgreUserRepository : WriteRepository<User>, IUserRepository
    {
        public PostgreUserRepository(AppDbContext dbContext) : base(dbContext)
        {
        }       
    }
}
