﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cmc.Infrastructure.Data.Connection
{
    public interface IConnectionStringProvider
    {
        string GetConnectionString();
    }
}
