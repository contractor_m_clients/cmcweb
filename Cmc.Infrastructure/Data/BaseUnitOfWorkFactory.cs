﻿using Cmc.Core.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Cmc.Infrastructure.Data
{
    public class BaseUnitOfWorkFactory: IUnitOfWorkFactory
    {
        private readonly IServiceProvider _provider;

        public BaseUnitOfWorkFactory(IServiceProvider provider)
        {
            _provider = provider;
        }

        public IUnitOfWork CreateUnitOfWork()
        {
            return _provider.GetService<IUnitOfWork>();
        }
    }
}
