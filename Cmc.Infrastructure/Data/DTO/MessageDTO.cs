﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cmc.Infrastructure.Data.DTO
{
    public class MessageDTO : DTO
    {
        public int Id { get; set; }
        public string Text { get; set; }
    }
}
