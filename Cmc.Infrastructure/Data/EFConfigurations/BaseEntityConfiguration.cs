﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Cmc.Entities.Base;

namespace Cmc.Infrastructure.Data.EFConfigurations
{
    public abstract class BaseEntityConfiguration<T> : IEntityTypeConfiguration<T> where T: BaseEntity
    {       
        public abstract void Configure(EntityTypeBuilder<T> builder);
    }
}
