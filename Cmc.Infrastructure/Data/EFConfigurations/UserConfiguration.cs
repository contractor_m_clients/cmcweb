﻿using Cmc.Entities.DBEntities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Cmc.Infrastructure.Data.EFConfigurations
{
    public class UserConfiguration : BaseEntityConfiguration<UserEntity>
    {
        public override void Configure(EntityTypeBuilder<UserEntity> builder)
        {
            builder.ToTable("Users");

            builder.HasKey(x => x.UserId);

            builder.Property(x => x.FirstName).HasColumnName("FirstName").HasMaxLength(100).IsRequired();
            builder.Property(x => x.LastName).HasColumnName("LastName").HasMaxLength(100).IsRequired();
            builder.Property(x => x.Age).HasColumnName("Age").IsRequired();
            builder.Property(x => x.DateOfBirth).HasColumnName("DateOfBirth");
        }
    }
}
