﻿using Microsoft.AspNetCore.Mvc;


namespace Cmc.Core.Interfaces
{
    public interface IPresenter<T>
    {
        void View(IResponse<T> response);
    }
}
