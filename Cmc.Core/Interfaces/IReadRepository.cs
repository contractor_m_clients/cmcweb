﻿using System.Linq;

namespace Cmc.Core.Interfaces
{
    public interface IReadRepository<T> where T : class
    {
        IQueryable<T> All { get; }
        T GetById(int id);
    }
}
