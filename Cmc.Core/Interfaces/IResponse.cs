﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cmc.Core.Interfaces
{
    public interface IResponse<T>
    {
        bool Result { get; set; }
        int StatusCode { get; set; }
        string Message { get; set; }
        T Data { get; set; }
    }
}
