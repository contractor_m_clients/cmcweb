﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cmc.Core.Interfaces
{
    public interface IUnitOfWorkFactory
    {
        IUnitOfWork CreateUnitOfWork();
    }
}
