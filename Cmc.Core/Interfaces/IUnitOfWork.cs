﻿using System;

namespace Cmc.Core.Interfaces
{
    public interface IUnitOfWork: IDisposable
    {
        void SaveChanges();
        void DiscartChanges();
        R GetNewRepo<R>();
    }
}
