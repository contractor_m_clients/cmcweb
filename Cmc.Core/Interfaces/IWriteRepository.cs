﻿
namespace Cmc.Core.Interfaces
{
    public interface IWriteRepository<T> : IReadRepository<T> where T : class
    {
        T Add(T entity);        
        void Update(T entity);
        void Delete(T entity);
    }
}