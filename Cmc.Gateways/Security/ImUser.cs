﻿using System;
using System.Collections.Generic;
using System.Text;
using Cmc.Entities;
using Cmc.Interactors.Interfaces;

namespace Cmc.Gateways
{
    class ImUsers : IGateway<int, User>
    {
        private static readonly Dictionary<int, User> _store = new Dictionary<int, User>();

        public User Get(int id)
        {
            return _store[id];
        }

        public User Save(User entity)
        {
            _store[entity.Id] = entity;
            return entity;
        }
    }
}
