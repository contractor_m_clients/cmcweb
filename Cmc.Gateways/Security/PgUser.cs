﻿using System;
using System.Collections.Generic;
using System.Text;
using Cmc.Entities;
using Cmc.Interactors.Interfaces;

namespace Cmc.Gateways
{
    class PgUser : IGateway<int, User>
    {
        public User Get(int id)
        {
            return new User();
        }

        public User Save(User entity)
        {
            return entity;
        }
    }
}
