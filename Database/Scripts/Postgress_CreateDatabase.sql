--
-- PostgreSQL database dump
--


SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: AspNetRoleClaims; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE "AspNetRoleClaims" (
    "Id" integer NOT NULL,
    "ClaimType" text,
    "ClaimValue" text,
    "RoleId" text NOT NULL
);


--
-- Name: AspNetRoleClaims_Id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "AspNetRoleClaims_Id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: AspNetRoleClaims_Id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "AspNetRoleClaims_Id_seq" OWNED BY "AspNetRoleClaims"."Id";


--
-- Name: AspNetRoles; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE "AspNetRoles" (
    "Id" text NOT NULL,
    "ConcurrencyStamp" text,
    "Name" character varying(256),
    "NormalizedName" character varying(256)
);


--
-- Name: AspNetUserClaims; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE "AspNetUserClaims" (
    "Id" integer NOT NULL,
    "ClaimType" text,
    "ClaimValue" text,
    "UserId" text NOT NULL
);


--
-- Name: AspNetUserClaims_Id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "AspNetUserClaims_Id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: AspNetUserClaims_Id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "AspNetUserClaims_Id_seq" OWNED BY "AspNetUserClaims"."Id";


--
-- Name: AspNetUserLogins; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE "AspNetUserLogins" (
    "LoginProvider" text NOT NULL,
    "ProviderKey" text NOT NULL,
    "ProviderDisplayName" text,
    "UserId" text NOT NULL
);


--
-- Name: AspNetUserRoles; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE "AspNetUserRoles" (
    "UserId" text NOT NULL,
    "RoleId" text NOT NULL
);


--
-- Name: AspNetUserTokens; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE "AspNetUserTokens" (
    "UserId" text NOT NULL,
    "LoginProvider" text NOT NULL,
    "Name" text NOT NULL,
    "Value" text
);


--
-- Name: AspNetUsers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE "AspNetUsers" (
    "Id" text NOT NULL,
    "AccessFailedCount" integer NOT NULL,
    "ConcurrencyStamp" text,
    "CoverImg" character varying(256),
    "Email" character varying(256),
    "EmailConfirmed" boolean NOT NULL,
    "Executor" boolean NOT NULL,
    "FirstName" character varying(256),
    "LastName" character varying(256),
    "LockoutEnabled" boolean NOT NULL,
    "LockoutEnd" timestamp with time zone,
    "NormalizedEmail" character varying(256),
    "NormalizedUserName" character varying(256),
    "PasswordHash" text,
    "PhoneNumber" text,
    "PhoneNumberConfirmed" boolean NOT NULL,
    "SecurityStamp" text,
    "TwoFactorEnabled" boolean NOT NULL,
    "UserName" character varying(256)
);


--
-- Name: Bet; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE "Bet" (
    "Id" integer NOT NULL,
    "Cost" money NOT NULL,
    "Date"  timestamp with time zone NOT NULL,
    "Duration" integer NOT NULL,
    "ProjectId" integer NOT NULL,
    "UserId" text,
    "Win" boolean NOT NULL,
    "WinDate"  timestamp with time zone
);


--
-- Name: Bet_Id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "Bet_Id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: Bet_Id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "Bet_Id_seq" OWNED BY "Bet"."Id";


--
-- Name: Category; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE "Category" (
    "Id" integer NOT NULL,
    "Name" character varying(256)
);


--
-- Name: Category_Id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "Category_Id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: Category_Id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "Category_Id_seq" OWNED BY "Category"."Id";


--
-- Name: Country; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE "Country" (
    "Id" integer NOT NULL,
    "Name" character varying(256)
);


--
-- Name: Country_Id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "Country_Id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: Country_Id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "Country_Id_seq" OWNED BY "Country"."Id";


--
-- Name: Grade; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE "Grade" (
    "Id" integer NOT NULL,
    "MaxValue" integer NOT NULL,
    "Name" character varying(256)
);


--
-- Name: Grade_Id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "Grade_Id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: Grade_Id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "Grade_Id_seq" OWNED BY "Grade"."Id";


--
-- Name: Group; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE "Group" (
    "Id" integer NOT NULL,
    "Name" character varying(256)
);


--
-- Name: GroupUser; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE "GroupUser" (
    "GroupId" integer NOT NULL,
    "UserId" text NOT NULL
);


--
-- Name: Group_Id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "Group_Id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: Group_Id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "Group_Id_seq" OWNED BY "Group"."Id";


--
-- Name: Message; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE "Message" (
    "Id" integer NOT NULL,
    "Description" text,
    "MessageType" integer NOT NULL,
    "ParentId" integer NOT NULL,
    "CreateDate" timestamp with time zone NOT NULL,
    "Readed" boolean NOT NULL,
    "Title" character varying(256) NOT NULL,
    "UserId" text
);


--
-- Name: Message_Id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "Message_Id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: Message_Id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "Message_Id_seq" OWNED BY "Message"."Id";


--
-- Name: PrivateMessage; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE "PrivateMessage" (
    "MessageId" integer NOT NULL,
    "UserId" text NOT NULL
);


--
-- Name: PrivateProjectMessage; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE "PrivateProjectMessage" (
    "MessageId" integer NOT NULL,
    "ProjectId" integer NOT NULL
);


--
-- Name: Project; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE "Project" (
    "Id" integer NOT NULL,
    "CategoryId" integer NOT NULL,
    "RegionId" integer NOT NULL,
    "UserId" text,
    "Title" character varying(256),
    "Description" text,
    "City" character varying(256),
    "Place" character varying(256),
    "ContactEmail" character varying(256),
    "ContactPhone" character varying(256),
    "EstimatedDuration" integer NOT NULL,
    "State" integer NOT NULL,
    "CreateDate" timestamp with time zone NOT NULL
);


--
-- Name: Project_Id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "Project_Id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: Project_Id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "Project_Id_seq" OWNED BY "Project"."Id";


--
-- Name: Region; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE "Region" (
    "Id" integer NOT NULL,
    "CountryId" integer NOT NULL,
    "Name" character varying(256)
);


--
-- Name: Region_Id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "Region_Id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: Region_Id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "Region_Id_seq" OWNED BY "Region"."Id";


--
-- Name: Review; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE "Review" (
    "Id" integer NOT NULL,
    "ProjectId" integer NOT NULL,
    "ReviewText" text,
    "UserId" text
);


--
-- Name: ReviewGrade; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE "ReviewGrade" (
    "ReviewId" integer NOT NULL,
    "GradeId" integer NOT NULL,
    "Value" integer NOT NULL
);


--
-- Name: Review_Id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "Review_Id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: Review_Id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "Review_Id_seq" OWNED BY "Review"."Id";



--
-- Name: AspNetRoleClaims Id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "AspNetRoleClaims" ALTER COLUMN "Id" SET DEFAULT nextval('"AspNetRoleClaims_Id_seq"'::regclass);


--
-- Name: AspNetUserClaims Id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "AspNetUserClaims" ALTER COLUMN "Id" SET DEFAULT nextval('"AspNetUserClaims_Id_seq"'::regclass);


--
-- Name: Bet Id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "Bet" ALTER COLUMN "Id" SET DEFAULT nextval('"Bet_Id_seq"'::regclass);


--
-- Name: Category Id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "Category" ALTER COLUMN "Id" SET DEFAULT nextval('"Category_Id_seq"'::regclass);


--
-- Name: Country Id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "Country" ALTER COLUMN "Id" SET DEFAULT nextval('"Country_Id_seq"'::regclass);


--
-- Name: Grade Id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "Grade" ALTER COLUMN "Id" SET DEFAULT nextval('"Grade_Id_seq"'::regclass);


--
-- Name: Group Id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "Group" ALTER COLUMN "Id" SET DEFAULT nextval('"Group_Id_seq"'::regclass);


--
-- Name: Message Id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "Message" ALTER COLUMN "Id" SET DEFAULT nextval('"Message_Id_seq"'::regclass);


--
-- Name: Project Id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "Project" ALTER COLUMN "Id" SET DEFAULT nextval('"Project_Id_seq"'::regclass);


--
-- Name: Region Id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "Region" ALTER COLUMN "Id" SET DEFAULT nextval('"Region_Id_seq"'::regclass);


--
-- Name: Review Id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "Review" ALTER COLUMN "Id" SET DEFAULT nextval('"Review_Id_seq"'::regclass);


--
-- Name: ReviewGrade AK_ReviewGrade_GradeId_ReviewId; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "ReviewGrade"
    ADD CONSTRAINT "AK_ReviewGrade_GradeId_ReviewId" UNIQUE ("GradeId", "ReviewId");


--
-- Name: AspNetRoleClaims PK_AspNetRoleClaims; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "AspNetRoleClaims"
    ADD CONSTRAINT "PK_AspNetRoleClaims" PRIMARY KEY ("Id");


--
-- Name: AspNetRoles PK_AspNetRoles; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "AspNetRoles"
    ADD CONSTRAINT "PK_AspNetRoles" PRIMARY KEY ("Id");


--
-- Name: AspNetUserClaims PK_AspNetUserClaims; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "AspNetUserClaims"
    ADD CONSTRAINT "PK_AspNetUserClaims" PRIMARY KEY ("Id");


--
-- Name: AspNetUserLogins PK_AspNetUserLogins; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "AspNetUserLogins"
    ADD CONSTRAINT "PK_AspNetUserLogins" PRIMARY KEY ("LoginProvider", "ProviderKey");


--
-- Name: AspNetUserRoles PK_AspNetUserRoles; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "AspNetUserRoles"
    ADD CONSTRAINT "PK_AspNetUserRoles" PRIMARY KEY ("UserId", "RoleId");


--
-- Name: AspNetUserTokens PK_AspNetUserTokens; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "AspNetUserTokens"
    ADD CONSTRAINT "PK_AspNetUserTokens" PRIMARY KEY ("UserId", "LoginProvider", "Name");


--
-- Name: AspNetUsers PK_AspNetUsers; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "AspNetUsers"
    ADD CONSTRAINT "PK_AspNetUsers" PRIMARY KEY ("Id");


--
-- Name: Bet PK_Bet; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "Bet"
    ADD CONSTRAINT "PK_Bet" PRIMARY KEY ("Id");


--
-- Name: Category PK_Category; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "Category"
    ADD CONSTRAINT "PK_Category" PRIMARY KEY ("Id");


--
-- Name: Country PK_Country; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "Country"
    ADD CONSTRAINT "PK_Country" PRIMARY KEY ("Id");


--
-- Name: Grade PK_Grade; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "Grade"
    ADD CONSTRAINT "PK_Grade" PRIMARY KEY ("Id");


--
-- Name: Group PK_Group; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "Group"
    ADD CONSTRAINT "PK_Group" PRIMARY KEY ("Id");


--
-- Name: GroupUser PK_GroupUser; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "GroupUser"
    ADD CONSTRAINT "PK_GroupUser" PRIMARY KEY ("GroupId", "UserId");


--
-- Name: Message PK_Message; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "Message"
    ADD CONSTRAINT "PK_Message" PRIMARY KEY ("Id");


--
-- Name: PrivateMessage PK_PrivateMessage; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "PrivateMessage"
    ADD CONSTRAINT "PK_PrivateMessage" PRIMARY KEY ("MessageId", "UserId");


--
-- Name: PrivateProjectMessage PK_PrivateProjectMessage; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "PrivateProjectMessage"
    ADD CONSTRAINT "PK_PrivateProjectMessage" PRIMARY KEY ("MessageId", "ProjectId");


--
-- Name: Project PK_Project; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "Project"
    ADD CONSTRAINT "PK_Project" PRIMARY KEY ("Id");


--
-- Name: Region PK_Region; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "Region"
    ADD CONSTRAINT "PK_Region" PRIMARY KEY ("Id");


--
-- Name: Review PK_Review; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "Review"
    ADD CONSTRAINT "PK_Review" PRIMARY KEY ("Id");


--
-- Name: ReviewGrade PK_ReviewGrade; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "ReviewGrade"
    ADD CONSTRAINT "PK_ReviewGrade" PRIMARY KEY ("ReviewId", "GradeId");



--
-- Name: EmailIndex; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "EmailIndex" ON "AspNetUsers" USING btree ("NormalizedEmail");


--
-- Name: IX_AspNetRoleClaims_RoleId; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "IX_AspNetRoleClaims_RoleId" ON "AspNetRoleClaims" USING btree ("RoleId");


--
-- Name: IX_AspNetUserClaims_UserId; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "IX_AspNetUserClaims_UserId" ON "AspNetUserClaims" USING btree ("UserId");


--
-- Name: IX_AspNetUserLogins_UserId; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "IX_AspNetUserLogins_UserId" ON "AspNetUserLogins" USING btree ("UserId");


--
-- Name: IX_AspNetUserRoles_RoleId; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "IX_AspNetUserRoles_RoleId" ON "AspNetUserRoles" USING btree ("RoleId");


--
-- Name: IX_Bet_ProjectId; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "IX_Bet_ProjectId" ON "Bet" USING btree ("ProjectId");


--
-- Name: IX_Bet_UserId; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "IX_Bet_UserId" ON "Bet" USING btree ("UserId");


--
-- Name: IX_GroupUser_UserId; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "IX_GroupUser_UserId" ON "GroupUser" USING btree ("UserId");


--
-- Name: IX_Message_UserId; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "IX_Message_UserId" ON "Message" USING btree ("UserId");


--
-- Name: IX_PrivateMessage_UserId; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "IX_PrivateMessage_UserId" ON "PrivateMessage" USING btree ("UserId");


--
-- Name: IX_PrivateProjectMessage_ProjectId; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "IX_PrivateProjectMessage_ProjectId" ON "PrivateProjectMessage" USING btree ("ProjectId");


--
-- Name: IX_Project_CategoryId; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "IX_Project_CategoryId" ON "Project" USING btree ("CategoryId");


--
-- Name: IX_Project_RegionId; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "IX_Project_RegionId" ON "Project" USING btree ("RegionId");


--
-- Name: IX_Project_UserId; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "IX_Project_UserId" ON "Project" USING btree ("UserId");


--
-- Name: IX_Region_CountryId; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "IX_Region_CountryId" ON "Region" USING btree ("CountryId");


--
-- Name: IX_Review_ProjectId; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "IX_Review_ProjectId" ON "Review" USING btree ("ProjectId");


--
-- Name: IX_Review_UserId; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "IX_Review_UserId" ON "Review" USING btree ("UserId");


--
-- Name: RoleNameIndex; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX "RoleNameIndex" ON "AspNetRoles" USING btree ("NormalizedName");


--
-- Name: UserNameIndex; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX "UserNameIndex" ON "AspNetUsers" USING btree ("NormalizedUserName");


--
-- Name: AspNetRoleClaims FK_AspNetRoleClaims_AspNetRoles_RoleId; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "AspNetRoleClaims"
    ADD CONSTRAINT "FK_AspNetRoleClaims_AspNetRoles_RoleId" FOREIGN KEY ("RoleId") REFERENCES "AspNetRoles"("Id") ON DELETE CASCADE;


--
-- Name: AspNetUserClaims FK_AspNetUserClaims_AspNetUsers_UserId; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "AspNetUserClaims"
    ADD CONSTRAINT "FK_AspNetUserClaims_AspNetUsers_UserId" FOREIGN KEY ("UserId") REFERENCES "AspNetUsers"("Id") ON DELETE CASCADE;


--
-- Name: AspNetUserLogins FK_AspNetUserLogins_AspNetUsers_UserId; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "AspNetUserLogins"
    ADD CONSTRAINT "FK_AspNetUserLogins_AspNetUsers_UserId" FOREIGN KEY ("UserId") REFERENCES "AspNetUsers"("Id") ON DELETE CASCADE;


--
-- Name: AspNetUserRoles FK_AspNetUserRoles_AspNetRoles_RoleId; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "AspNetUserRoles"
    ADD CONSTRAINT "FK_AspNetUserRoles_AspNetRoles_RoleId" FOREIGN KEY ("RoleId") REFERENCES "AspNetRoles"("Id") ON DELETE CASCADE;


--
-- Name: AspNetUserRoles FK_AspNetUserRoles_AspNetUsers_UserId; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "AspNetUserRoles"
    ADD CONSTRAINT "FK_AspNetUserRoles_AspNetUsers_UserId" FOREIGN KEY ("UserId") REFERENCES "AspNetUsers"("Id") ON DELETE CASCADE;


--
-- Name: AspNetUserTokens FK_AspNetUserTokens_AspNetUsers_UserId; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "AspNetUserTokens"
    ADD CONSTRAINT "FK_AspNetUserTokens_AspNetUsers_UserId" FOREIGN KEY ("UserId") REFERENCES "AspNetUsers"("Id") ON DELETE CASCADE;


--
-- Name: Bet FK_Bet_AspNetUsers_UserId; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "Bet"
    ADD CONSTRAINT "FK_Bet_AspNetUsers_UserId" FOREIGN KEY ("UserId") REFERENCES "AspNetUsers"("Id");


--
-- Name: Bet FK_Bet_Project_ProjectId; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "Bet"
    ADD CONSTRAINT "FK_Bet_Project_ProjectId" FOREIGN KEY ("ProjectId") REFERENCES "Project"("Id") ON DELETE CASCADE;


--
-- Name: GroupUser FK_GroupUser_AspNetUsers_UserId; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "GroupUser"
    ADD CONSTRAINT "FK_GroupUser_AspNetUsers_UserId" FOREIGN KEY ("UserId") REFERENCES "AspNetUsers"("Id") ON DELETE CASCADE;


--
-- Name: GroupUser FK_GroupUser_Group_GroupId; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "GroupUser"
    ADD CONSTRAINT "FK_GroupUser_Group_GroupId" FOREIGN KEY ("GroupId") REFERENCES "Group"("Id") ON DELETE CASCADE;


--
-- Name: Message FK_Message_AspNetUsers_UserId; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "Message"
    ADD CONSTRAINT "FK_Message_AspNetUsers_UserId" FOREIGN KEY ("UserId") REFERENCES "AspNetUsers"("Id");


--
-- Name: PrivateMessage FK_PrivateMessage_AspNetUsers_UserId; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "PrivateMessage"
    ADD CONSTRAINT "FK_PrivateMessage_AspNetUsers_UserId" FOREIGN KEY ("UserId") REFERENCES "AspNetUsers"("Id") ON DELETE CASCADE;


--
-- Name: PrivateMessage FK_PrivateMessage_Message_MessageId; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "PrivateMessage"
    ADD CONSTRAINT "FK_PrivateMessage_Message_MessageId" FOREIGN KEY ("MessageId") REFERENCES "Message"("Id") ON DELETE CASCADE;


--
-- Name: PrivateProjectMessage FK_PrivateProjectMessage_Message_MessageId; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "PrivateProjectMessage"
    ADD CONSTRAINT "FK_PrivateProjectMessage_Message_MessageId" FOREIGN KEY ("MessageId") REFERENCES "Message"("Id") ON DELETE CASCADE;


--
-- Name: PrivateProjectMessage FK_PrivateProjectMessage_Project_ProjectId; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "PrivateProjectMessage"
    ADD CONSTRAINT "FK_PrivateProjectMessage_Project_ProjectId" FOREIGN KEY ("ProjectId") REFERENCES "Project"("Id") ON DELETE CASCADE;


--
-- Name: Project FK_Project_AspNetUsers_UserId; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "Project"
    ADD CONSTRAINT "FK_Project_AspNetUsers_UserId" FOREIGN KEY ("UserId") REFERENCES "AspNetUsers"("Id");


--
-- Name: Project FK_Project_Category_CategoryId; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "Project"
    ADD CONSTRAINT "FK_Project_Category_CategoryId" FOREIGN KEY ("CategoryId") REFERENCES "Category"("Id") ON DELETE CASCADE;


--
-- Name: Project FK_Project_Region_RegionId; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "Project"
    ADD CONSTRAINT "FK_Project_Region_RegionId" FOREIGN KEY ("RegionId") REFERENCES "Region"("Id") ON DELETE CASCADE;


--
-- Name: Region FK_Region_Country_CountryId; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "Region"
    ADD CONSTRAINT "FK_Region_Country_CountryId" FOREIGN KEY ("CountryId") REFERENCES "Country"("Id") ON DELETE CASCADE;


--
-- Name: ReviewGrade FK_ReviewGrade_Grade_GradeId; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "ReviewGrade"
    ADD CONSTRAINT "FK_ReviewGrade_Grade_GradeId" FOREIGN KEY ("GradeId") REFERENCES "Grade"("Id") ON DELETE CASCADE;


--
-- Name: ReviewGrade FK_ReviewGrade_Review_ReviewId; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "ReviewGrade"
    ADD CONSTRAINT "FK_ReviewGrade_Review_ReviewId" FOREIGN KEY ("ReviewId") REFERENCES "Review"("Id") ON DELETE CASCADE;


--
-- Name: Review FK_Review_AspNetUsers_UserId; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "Review"
    ADD CONSTRAINT "FK_Review_AspNetUsers_UserId" FOREIGN KEY ("UserId") REFERENCES "AspNetUsers"("Id");


--
-- Name: Review FK_Review_Project_ProjectId; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "Review"
    ADD CONSTRAINT "FK_Review_Project_ProjectId" FOREIGN KEY ("ProjectId") REFERENCES "Project"("Id") ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

