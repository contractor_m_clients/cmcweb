﻿using Cmc.Core.Events;
using Cmc.Core.Interfaces;

namespace Cmc.Core.Services
{
    public class ToDoItemService : IHandle<ToDoItemCompletedEvent>
    {
        public void Handle(ToDoItemCompletedEvent domainEvent)
        {
            // Do Nothing
        }
    }
}
