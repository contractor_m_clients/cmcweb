﻿using Cmc.Core.Interfaces;
using Cmc.Core.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Text;
using Cmc.Core.Entities;
using System.Linq;
using Cmc.Infrastructure.Data.Repositories;

namespace Cmc.Services.Items
{
    public class UserService: IUserService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public UserService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public IList<User> GetActiveUsers()
        {
            using (var unitOfWork = _unitOfWorkFactory.CreateUnitOfWork())
            {
                var repo1 = unitOfWork.GetNewRepo<WriteRepository<User>>();
                var repo2 = unitOfWork.GetNewRepo<WriteRepository<User>>();
                repo1.All.ToList();
                repo2.All.ToList();
            }

            using (var unitOfWork = _unitOfWorkFactory.CreateUnitOfWork())
            {
                var repo3 = unitOfWork.GetNewRepo<WriteRepository<User>>();
                var repo4 = unitOfWork.GetNewRepo<WriteRepository<User>>();
                repo3.All.ToList();
                return repo4.All.ToList();
            }
        }
    }
}
